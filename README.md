# defaults

## Instructions

source default scripts directly from this repo in your standard DeMi scripts
of the same name.

```shell
export DEFAULTS_URL https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/defaults/-/raw/master/
```
example:

Using [cURL](https://curl.se/)
```shell
curl -s "${DEFAULTS_URL}/config"
```
or [wget](https://www.gnu.org/software/wget/)
```shell
wget -qO - "${DEFAULTS_URL}/config"
```

You can also clone this repository and access these scripts with the `file:///` descriptor.

## Contributing

be careful when modifying the default scripts as they are sourced in most of the
other `itk-demo-sw` repos.

